function fetchCocktails() {
    document.getElementById('cocktail-list').innerHTML = '';

    const alcoholType = document.querySelector('input[name="alcoholType"]:checked').value;

    fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${alcoholType}`)
        .then(response => response.json())
        .then(data => {
            const cocktailList = document.getElementById('cocktail-list');

            const cocktailImagesDiv = document.createElement('div');
            cocktailImagesDiv.classList.add('cocktail-images');

            data.drinks.forEach((cocktail, index) => {
                const cocktailDiv = document.createElement('div');
                cocktailDiv.classList.add('cocktail-col');

                const cocktailName = document.createElement('h3');
                cocktailName.textContent = cocktail.strDrink;

                if (cocktail.strDrinkThumb) {
                    const cocktailImage = document.createElement('img');
                    cocktailImage.src = cocktail.strDrinkThumb;
                    cocktailImage.alt = cocktail.strDrink;
                    cocktailDiv.appendChild(cocktailImage);
                }

                cocktailDiv.appendChild(cocktailName);
                cocktailImagesDiv.appendChild(cocktailDiv);

                if ((index + 1) % 4 === 0) {
                    const cocktailRow = document.createElement('div');
                    cocktailRow.classList.add('cocktail-row');
                    cocktailRow.appendChild(cocktailImagesDiv.cloneNode(true));
                    cocktailList.appendChild(cocktailRow);
                    cocktailImagesDiv.innerHTML = ''; 
                }
            });

            document.getElementById('totalDrinks').textContent = data.drinks.length;
        })
        .catch(error => console.error('Error al obtener la lista de cócteles:', error));
}

function clearCocktails() {
    document.getElementById('cocktail-list').innerHTML = '';
    document.getElementById('totalDrinks').textContent = '0';
}

document.addEventListener('DOMContentLoaded', () => {
    fetchCocktails();
});